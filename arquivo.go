package main

import (
	"log"
	"net/http"
	"time"
)

func RunService() {
	s := &http.Server{
		Addr:         "172.22.51.15:8080",
		Handler:      Router{},
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	log.Fatal(s.ListenAndServe())
}
